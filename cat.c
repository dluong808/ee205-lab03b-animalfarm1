///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @January 31 2021
///////////////////////////////////////////////////////////////////////////////

#include <string.h>

#include <stdio.h>
#include "animals.h"
#include "cat.h"

// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
struct Cat catDB[MAX_SPECIES];

/// Add Alice to the Cat catabase at position i.
/// 
//
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) {
   struct Cat alice;
   strcpy(alice.name, "Alice");
   alice.gender = FEMALE;
   alice.breed = MAIN_COON;
   alice.isFixed = true;
   alice.weight = 200;
   alice.collar1_color = BLUE;
   alice.collar2_color = WHITE;
   alice.license = 232223;
   catDB[i]=alice;
}

char* breedName (enum catBreeds breed){

   switch (breed){

      case 0:
         return "MAINE COON";
      case 1:
         return "MANX";
      case 2:
         return "SHORHAIR";
      case 3:
         return "PERSIAN";
      case 4:
        return "SPHYNX";}
}



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
   // Here's a clue of what one printf() might look like...
 // printf ("    collar color 1 = [%s]\n", colorName( catDB[i].collar1_color ));
   printf (" Cat name = [%s]\n", catDB[i].name);
   printf ("     gender = [%s]\n",genderName(catDB[i].gender));
   printf ("     breed = [%s]\n", breedName(catDB[i].breed));
   printf ("     isFixed = [%s]\n",fixedString(catDB[i].isFixed));
   printf ("     weight = [%f]\n",catDB[i].weight);
   printf ("     collar color 1 = [%s]\n",colorName(catDB[i].collar1_color));
   printf ("     collar color 2 = [%s]\n",colorName(catDB[i].collar2_color));
   printf ("     license = [%ld]\n",catDB[i].license);
}

