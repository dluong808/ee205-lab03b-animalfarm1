###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author @Daniel Luong<dluong@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   @January 31 2021
###############################################################################
CC		= gcc
CFLAGS = -g
TARGET = animalfarm

all: $(TARGET)

$(TARGET): main.o cat.o animals.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o cat.o animals.o

main.o: main.c cat.h animals.h
	$(CC) $(CFLAGS) -c main.c

cat.o: cat.c cat.h animals.h
	$(CC) $(CFLAGS) -c cat.c

animals.o: animals.c  animals.h
	$(CC) $(CFLAGS) -c animals.c


clean:
	rm -f *.o animalfarm
