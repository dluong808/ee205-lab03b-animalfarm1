///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @January 31 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"


//Represents the enum Gender into strings for printf

char* genderName (enum Gender gender){

   switch (gender)
   {
      case MALE:
         return "MALE";
      case FEMALE:
         return "FEMALE";
   }
}

char* fixedString (bool isFixed){
   switch (isFixed)
   {
      case false:
         return "NO";
      case true:
         return "YES";
   }
}

/// Decode the enum Color into strings for printf()
// char* colorName (enum Color color) {
char* colorName (enum Color color){
   
   switch (color)
   {
      case BLACK:
         return "BLACK";
      case WHITE:
         return "WHITE";
      case RED:
         return "RED";
      case BLUE:
         return "BLUE";
      case GREEN:
         return "GREEN";
      case PINK:
         return "PINK";
      }

}







   // @todo Map the enum Color to a string

//   return NULL; // We should never get here
// };


