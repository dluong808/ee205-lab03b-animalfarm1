///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @January 31 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <stdbool.h>
/// Define the maximum number of cats or dogs in our array-database
#define MAX_SPECIES (20)

/// Gender is appropriate for all animals in this database
// enum Gender // @todo fill this out from here...
enum Color {BLACK,WHITE,RED,BLUE,GREEN,PINK};
enum Gender {MALE,FEMALE};

/// Return a string for the name of the color
/// @todo this is for you to implement
// char* colorName (enum Color color);
//
char* genderName (enum Gender gendeR);

char* fixedString (bool isFixed);

char* colorName (enum Color color);
